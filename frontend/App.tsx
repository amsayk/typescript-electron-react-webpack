import * as React from 'react';
import Counter from './Counter';

export default () => {
  return (
    <div>
      <div className='logoBar'>
        <img src={require('./assets/images/logos/webpack.png')} />
        <img src={require('./assets/images/logos/electron.png')} />
        <img src={require('./assets/images/logos/react.png')} />
      </div>

      Node version: {process.versions.node}<br/>
      <Counter/>
    </div>
  );
};
