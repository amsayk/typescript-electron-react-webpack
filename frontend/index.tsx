import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';

require('./assets/styles/app.scss');

const renderApp = () => {
  const App = require('./App').default;

  ReactDOM.render(
    (
      <AppContainer>
        <App />
      </AppContainer>
    ),
    document.getElementById('root')
  );
};

renderApp();

if (module.hot) {
  module.hot.accept('./App', renderApp);
}
