const useref = require('useref');

module.exports = class WebpackUseref {
  apply(compiler) {
    compiler.plugin('compilation', function (compilation) {
      compilation.plugin('html-webpack-plugin-before-html-processing', function (htmlPluginData, callback) {
        console.log('[webpack-useref-plugin] - Processing');
        let result = useref(htmlPluginData.html);

        htmlPluginData.html = result[ 0 ];
        callback(null, htmlPluginData);
      })
    });
  }
};
