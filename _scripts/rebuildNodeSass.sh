#!/bin/sh

export npm_config_target=1.8.0
export npm_config_arch=x64
export npm_config_disturl=https://atom.io/download/atom-shell
export npm_config_runtime=electron
export npm_config_build_from_source=true

cd node_modules/node-sass
rm -rf vendor/[win,darwin]*

npm run build
