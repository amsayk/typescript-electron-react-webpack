/**
 * Simple electron-connect server to reload on dist-file changes.
 */

const path = require('path');
const fs = require('fs');
const chalk = require('chalk');
const electronConnect = require('electron-connect');
const { spawn } = require('child_process');

const watchFile = path.resolve(__dirname, '..', 'dist', 'backend.js');

let electron;

const runElectron = () => {
  electron = electronConnect.server.create({
    path: path.dirname(watchFile)
  });

  fs.watchFile(watchFile, electron.restart);

  electron.start();
};

console.log(chalk.green('Starting backend watch cycle...'));

spawn('npm', [
  'run', 'build:dev:backend', '--', '--watch'
], {
  env: { hot: 1 },
  stdio: 'inherit',
  shell: true
});

// Wait 10 seconds before starting Electron
setTimeout(runElectron, 10 * 1000);
