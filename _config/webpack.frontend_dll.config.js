const path = require('path');
const fs = require('fs');
const chalk = require('chalk');
const merge = require('webpack-merge');
const { BuildTimeDefs, Plugins, BaseConfigs } = require('./webpack.definitions');
const { dependencies } = BuildTimeDefs.PackageInfo;
const forceBuild = process.env.FORCE_BUILD_DLL == true || process.env.FORCE_BUILD_DLL === 'true';

/**
 * Exit early if DLL already exists
 */
if (!forceBuild && fs.existsSync(BuildTimeDefs.DLLManifestPath)) {
  console.log(chalk.black.bgYellow.bold(
    'The DLL files already exist.\n' +
    'If you want to rebuild, first clean with "npm run clean" and re-run this build!\n' +
    'Alternatively you can enable env var FORCE_BUILD_DLL=1'
  ));

  process.exit(0);
}

module.exports = merge(
  BaseConfigs.CommonConfig,
  {
    target: 'electron-renderer',
    entry: Object.keys(dependencies || {}),
    output: {
      filename: path.join(BuildTimeDefs.Outputs.Scripts, `${BuildTimeDefs.FrontEndName}_dll.js`),
      library: `${BuildTimeDefs.FrontEndName}_dll`,
      libraryTarget: 'var'
    },
    plugins: [
      Plugins.DLL
    ]
  }
);
