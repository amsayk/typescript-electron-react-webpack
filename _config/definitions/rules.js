const path = require('path');

const ExtractTextPlugin = require('extract-text-webpack-plugin');

const BuildTimeInfo = require('./buildTime');

/**
 * Copy PackageJSON to output via `file-loader`
 */
const PackageJSON = {
  test: /package.json$/,
  exclude: /node_modules/,
  use: [
    {
      loader: 'file-loader',
      options: {
        name: 'package.json'
      }
    }
  ]
};

/**
 * Pre-Process TypeScript files with `tslint`
 */
const TypeScript = {
  test: /\.ts$/,
  enforce: 'pre',
  loader: 'tslint-loader',
  options: {
    typeCheck: true,
    emitErrors: true
  }
};

/**
 * Transpile TS[X] files
 */
const TSX = {
  test: /\.tsx?$/,
  use: [
    {
      loader: 'awesome-typescript-loader',
      options: {
        useCache: true
      }
    }
  ]
};

/**
 * Pre-Process JS files to enhance SourceMaps
 * TODO: See if this even works...
 */
const SourceMaps = {
  enforce: 'pre',
  test: /\.js$/,
  loader: 'source-map-loader'
};

const SassExtract = {
  test: /\.scss$/,
  use: ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: [
      {
        loader: 'css-loader',
        options: {
          sourceMap: !BuildTimeInfo.isProd
        }
      },
      'resolve-url-loader',
      {
        loader: 'sass-loader',
        options: {
          sourceMap: !BuildTimeInfo.isProd
        }
      }
    ]
  })
};

const SassDynamic = {
  test: /\.scss$/,
  use: [
    'style-loader',
    {
      loader: 'css-loader',
      options: {
        sourceMap: !BuildTimeInfo.isProd
      }
    },
    'resolve-url-loader',
    {
      loader: 'sass-loader',
      options: {
        sourceMap: !BuildTimeInfo.isProd
      }
    }
  ]
};

const Images = {
  test: /\.(png|svg|jpg|gif)$/,
  use: [
    {
      loader: 'file-loader',
      options: {
        name: path.posix.join(BuildTimeInfo.Outputs.Images, '[name].[ext]')
      }
    }
  ]
};

const Fonts = {
  test: /\.(woff|woff2|eot|ttf|otf)$/,
  use: [
    {
      loader: 'file-loader',
      options: {
        name: path.posix.join(BuildTimeInfo.Outputs.Fonts, '[name].[ext]')
      }
    }
  ]
};

module.exports = {
  Fonts,
  Images,
  PackageJSON,
  SassDynamic,
  SassExtract,
  SourceMaps,
  TypeScript,
  TSX
};
