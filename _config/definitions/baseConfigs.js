const webpack = require('webpack');
const merge = require('webpack-merge');
const UglifyEsPlugin = require('uglifyjs-webpack-plugin');
const WebpackUseref = require('../../_scripts/webpack-useref-plugin');

const { WDSPort } = require('./buildTime');

const BuildTimeInfo = require('./buildTime');
const WebPackCfg = require('./webpack');
const Rules = require('./rules');
const Plugins = require('./plugins');

/**
 * Config for Common values across all bundles
 */
const CommonConfig = {
  output: WebPackCfg.Output,
  resolve: WebPackCfg.Resolver,
  node: {
    __dirname: false,
    __filename: false
  },
  module: {
    rules: [
      Rules.Fonts,
      Rules.Images,
      Rules.PackageJSON,
      BuildTimeInfo.isHot ?
        Rules.SassDynamic :
        Rules.SassExtract,
      Rules.TypeScript,
      Rules.TSX
    ]
  },
  plugins: [
    Plugins.DefineConstants,
    Plugins.ExtractCSS
  ]
};

/**
 * Config modifier for Dev bundles.
 */
const DevConfigMod = {
  frontend: {
    // Add SourceMaps
    devtool: 'inline-source-map',
    module: {
      rules: [
        Rules.SourceMaps
      ]
    },
    plugins: [
      Plugins.HTMLIncludeAssets,
      Plugins.DLLReference
    ]
  }
};


/**
 * Config modifier for Dev bundles.
 */
const HotConfigMod = {
  // Add RHL into entries (prepend strategy)
  entry: [
    `webpack-dev-server/client?http://0.0.0.0:${WDSPort}/`,
    'webpack/hot/only-dev-server',
    'react-hot-loader/patch'
  ],

  output: {
    publicPath: `http://localhost:${WDSPort}/`
  },

  // Add HMR into Loaders for TSX rule (prepend strategy)
  module: {
    rules: [
      merge.strategy({
        use: 'prepend'
      })
      (
        Rules.TSX,
        {
          use: [
            'react-hot-loader/webpack'
          ]
        }
      )
    ]
  },

  // Add HMR Plugin for module.hot access
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ]
};

/**
 * Config modifier for Prod bundles.
 */
const ProdConfigMod = {
  frontend: {
    plugins: [
      // Enable useref functionality
      new WebpackUseref(),
      // Uglify ES6 (minification)
      new UglifyEsPlugin()
    ]
  },
  backend: {
    plugins: [
      // Uglify ES6 (minification)
      new UglifyEsPlugin()
    ]
  }
};

module.exports = {
  CommonConfig,
  DevConfigMod,
  HotConfigMod,
  ProdConfigMod
};
