const path = require('path');

const isProd = process.env.NODE_ENV === 'production';
const isHot = process.env.HOT == true || process.env.HOT == 'true';
const configOnly = process.env.SHOW_CONFIG_ONLY == true || process.env.SHOW_CONFIG_ONLY == 'true';
const WDSPort = process.env.WDSPORT || 3000;
const frontEndName = 'frontend';
const backEndName = 'backend';
const rootDir = path.resolve(process.cwd());
const packageJSONPath = path.resolve(rootDir, 'package.json');
const HtmlTemplatePath = path.resolve(rootDir, 'frontend', 'index.html');
const PackageInfo = isHot ? {} : JSON.parse(require('fs').readFileSync(packageJSONPath));

// Path configurations
const distDir = path.resolve(rootDir, 'dist');
const assetsDir = path.resolve(distDir, 'assets');
// Use Relative conventions for `filename` uses
const relativeAssets = path.relative(distDir, assetsDir);
const scriptsDir = path.posix.join(relativeAssets, 'js');
const stylesDir = path.posix.join(relativeAssets, 'styles');
const imagesDir = path.posix.join(relativeAssets, 'images');
const fontsDir = path.posix.join(relativeAssets, 'fonts');

const DLLManifestPath = path.resolve(distDir, scriptsDir, `${frontEndName}_dll.json`);

const buildCfg = {
  isProd,
  isHot,
  configOnly,
  FrontEndName: frontEndName,
  BackEndName: backEndName,
  RootDir: rootDir,
  DLLManifestPath,
  HtmlTemplatePath,
  PackageInfo,
  WDSPort,

  Outputs: {
    Root: distDir,
    Assets: assetsDir,
    Scripts: scriptsDir,
    Styles: stylesDir,
    Images: imagesDir,
    Fonts: fontsDir
  }
};

// console.log(require('util').inspect(buildCfg, false, 99));
// process.exit(0);

module.exports = buildCfg;
