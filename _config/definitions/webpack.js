const path = require('path');

const { Outputs } = require('./buildTime');

/**
 * Output bundle destination
 */
const Output = {
  path: Outputs.Root,
  pathinfo: true
};

/**
 * Resolve definitions for WebPack; particularly extensions that will resolve modules.
 */
const Resolver = {
  extensions: [ '.js', '.ts', '.tsx', '.jsx', '.json' ]
};

const WatchOptions = {
  backend: {
    watchOptions: {
      ignored: [
        /dist/,
        /node_modules/
      ]
    }
  }
};

module.exports = {
  Output,
  Resolver,
  WatchOptions
};
