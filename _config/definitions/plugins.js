const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackIncludeAssetsPlugin = require('html-webpack-include-assets-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const BuildTimeInfo = require('./buildTime');

/**
 * Define Production mode via `DefinePlugin`
 * Replaces `process.env` with the string 'production'
 * NOTE: This is mainly for Uglifying React.js.
 *       UglifyES will remove falsey blocks to elimate extra code.
 */
const DefineConstants = new webpack.DefinePlugin({
  'process.env': {
    NODE_ENV: JSON.stringify(BuildTimeInfo.isProd ? 'production' : 'development'),
    START_PROTOCOL: JSON.stringify(BuildTimeInfo.isHot ? 'http:' : 'file:'),
    START_FILE: JSON.stringify(BuildTimeInfo.isHot ? `localhost:${BuildTimeInfo.WDSPort}` : './index.html'),
    HOT: JSON.stringify(BuildTimeInfo.isHot),
    WDS_PORT: BuildTimeInfo.WDSPort
  }
});

/**
 * HTML Plugin to generate `index.html`
 * NOTE: If Production is detected, it will minify the HTML.
 */
const WebPackHTML = new HtmlWebpackPlugin({
  title: BuildTimeInfo.PackageInfo.name,
  template: BuildTimeInfo.HtmlTemplatePath,
  hash: true,
  minify: !BuildTimeInfo.isProd ? {} : {
    collapseWhitespace: true
  }
});

/**
 * HTML Plugin Modifier to add DLL as script tag.
 */
const HTMLIncludeAssets = new HtmlWebpackIncludeAssetsPlugin({
  assets: [ path.join(BuildTimeInfo.Outputs.Scripts, `${BuildTimeInfo.FrontEndName}_dll.js`) ],
  append: false
});

/**
 * DLL Plugin to include F/E dependencies
 */
const DLL = new webpack.DllPlugin({
  name: `${BuildTimeInfo.FrontEndName}_dll`,
  path: BuildTimeInfo.DLLManifestPath
});

/**
 * DLLReference Plugin to reference F/E dependencies
 * not compiled in dev build
 */
const DLLReference = new webpack.DllReferencePlugin({
  context: BuildTimeInfo.RootDir,
  manifest: BuildTimeInfo.DLLManifestPath,
  sourceType: 'var',
});

const ExtractCSS = new ExtractTextPlugin(`${BuildTimeInfo.Outputs.Styles}/app.css`);

module.exports = {
  DefineConstants,
  DLL,
  DLLReference,
  ExtractCSS,
  HTMLIncludeAssets,
  WebPackHTML
};
