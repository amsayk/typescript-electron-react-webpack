const path = require('path');
const fs = require('fs');
const chalk = require('chalk');
const merge = require('webpack-merge');
const { BuildTimeDefs, Plugins, BaseConfigs } = require('./webpack.definitions');

/**
 * Warn and build DLL if it doesn't exist
 */
if (!BuildTimeDefs.isProd && !fs.existsSync(BuildTimeDefs.DLLManifestPath)) {
  console.log(chalk.yellow.bgRed.bold(
    'The DLL files are missing. Please build them with "npm run build:dev:frontend_dll" before running this build!'
  ));

  process.exit(99);
}

const config = merge.smart(
  // Common Configuration
  BaseConfigs.CommonConfig,

  // Add Dev/Prod config modifiers
  BuildTimeDefs.isProd ?
    BaseConfigs.ProdConfigMod.frontend :
    BaseConfigs.DevConfigMod.frontend,

  // Add HMR if needed
  BuildTimeDefs.isHot ?
    BaseConfigs.HotConfigMod : {},

  // Build-specific configuration
  {
    target: 'electron-renderer',
    entry: [ './frontend/index.tsx' ],
    output: {
      filename: path.join(BuildTimeDefs.Outputs.Scripts, `${BuildTimeDefs.FrontEndName}.js`),
    },
    plugins: [
      Plugins.WebPackHTML
    ]
  }
);

if (BuildTimeDefs.configOnly) {
  console.log(require('chalk').bgGreen(require('util').inspect(config, false, null)));
  process.exit(0);
}

module.exports = config;
