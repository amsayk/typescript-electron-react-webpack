const path = require('path');
const merge = require('webpack-merge');
const nodeExternals = require('webpack-node-externals');
const { BuildTimeDefs, BaseConfigs, Webpack } = require('./webpack.definitions');

const config = merge(
  // Common Configuration
  BaseConfigs.CommonConfig,

  // Add Dev/Prod config modifiers
  BuildTimeDefs.isProd ?
    BaseConfigs.ProdConfigMod.backend :
    BaseConfigs.DevConfigMod.backend,

  Webpack.WatchOptions.backend,

  // Build-specific configuration
  {
    target: 'electron-main',
    entry: [ './backend/index.ts' ],
    output: {
      filename: `${BuildTimeDefs.BackEndName}.js`
    },
    externals: [
      nodeExternals()
    ]
  }
);

if (BuildTimeDefs.configOnly) {
  console.log(require('chalk').bgGreen(require('util').inspect(config, false, null)));
  process.exit(0);
}

module.exports = config;
