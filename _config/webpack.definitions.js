/**
 * Modular roll-up of various WebPack configurations.
 *
 * This allows for the configuration file to be more legible.
 */

const BaseConfigs = require('./definitions/baseConfigs');
const BuildTimeDefs = require('./definitions/buildTime');
const Plugins = require('./definitions/plugins');
const Rules = require('./definitions/rules');
const Webpack = require('./definitions/webpack');

module.exports = {
  BaseConfigs,
  BuildTimeDefs,
  Plugins,
  Rules,
  Webpack
};
