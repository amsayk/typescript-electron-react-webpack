#typescript-electron-react-webpack

This project is an attempt at building a flexible build system with the following technology:

* [Typescript](https://www.typescriptlang.org/)
* [Electron](https://electron.atom.io/)
* [React](https://facebook.github.io/react)
* [webpack](https://webpack.github.io/)
* [electron-connect](https://github.com/Quramy/electron-connect)

## Pieces of the proverbial pie

This application is split into two parts: the Backend, and the Frontend.

While you can do anything you want in the Frontend that you could in the Backend (because this is Electron), 
I like to treat my apps as if they were built in a standard fashion where they are split.

### Build Information

Also because of the logical split, `webpack` has two separate builds - but this is managed in a modular configuration.

Abstracted settings definitions are stored in `webpack.definitions.js`, including build-time modifiers (i.e., plugins/rules necessary for dev vs prod). This allows for configurations to be more legible and quickly glance at what is actually occurring without the large amount of configuration blinding you from the high-level overview.

Upon installation, `npm run build:dev:frontend_dll` is automatically ran to speed up the time it takes to start developing!

## Commands

| Command | Description |
| ---------- | ---------- |
| `npm install` | Install necessary dependencies. Also compiles `frontend_dll`. |
| `npm run clean` | Deletes the `dist` output, and any build cache involved. |
| `npm run lint` | Run TypeScript linter. |
| `npm run test` | Run tests. (TBD) |
| `npm run build:dev` | Builds all parts of the project for Development. |
| `npm run build:dev:frontend` | Builds only the FrontEnd bundle for Development. |
| `npm run build:dev:frontend_dll` | Builds only the FrontEnd DLL bundle for Development. |
| `npm run build:dev:backend` | Builds only the BackEnd bundle for Development. |
| `npm run build:prod` | Builds all parts of the project for Production. |
| `npm run build:prod:frontend` | Builds only the FrontEnd bundle for Production. |
| `npm run build:prod:backend` | Builds only the BackEnd bundle for Production. |
| `npm run dev` | Starts up `electron-connect` for Live Reload of Electron. |
| `npm start` | Starts up `electron` project in the `dist` output. |

## Env Vars

Boolean values are meant to be set to `1` or `true`

| VAR | Description |
| ---------- | ---------- |
| `NODE_ENV` | `development`&#124;`production`; Can mostly be ignored, as build tasks set these for you. |
| `FORCE_BUILD_DLL` | `Boolean` Forces the build of `frontend_dll` should files already exist. Useful for when you change your core dependencies. |
| `HOT` | `Boolean` Enables Hot Module Reloading, `webpack-dev-server`, and `react-hot-load` for development. Using this, you do not need to compiled the `frontend` targets. |
| `WDSPORT` | `Int` Defines the port to run `webpack-dev-server` on. Defaults to `3000` |
| `UPGRADE_EXTENSIONS` | `Boolean` Forces a download/install of devtool extensions via `electron-devtools-installer`. |

## Development Workflow

There's a few train of thoughts you can follow when developing: Standard dev refresh, or Hot Module Replacement, and Electron Live Reload.

In both cases, the `frontend_dll` will need to be built. This is *usually* done for you upon `npm install`, but should you need to generate it the build process will inform you.

### Standard Development

You simply need to run the development targets:

```
npm run build:dev
```

or

```
npm run build:dev:backend
npm run build:dev:frontend
```

Then start the project as normal:

```
npm start
```

### HMR Development

Similar to Standard Development, you run a development target, but with an Environment Variable defining the usage of HMR.

However, you do not need to compile the `frontend` target, as it will be dynamically compiled by WebPack as changes are detected.

```
HOT=1 npm run build:dev:backend
npm start
```

### Electron Live Reload

In conjunction with *HMR Development*, you can run a Live Reload version of the Electron runtime that will restart the main process when changes are detected.

This is managed for you, and you simply need to run the special target:

```
npm run dev
```

## Building for Production

In this boilerplate, Production builds:
 
* include *all* dependencies (unlike dev where they're pre-compiled into a `dll` file).
* are minified via `UglifyES`.
* run `useref` on the `index.html` file (for excluding development scripts if necessary)

```
npm run build:prod
npm start
```
