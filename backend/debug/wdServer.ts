const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const config = require('../../_config/webpack.frontend.config');
const WDSPort = process.env.WDS_PORT;

module.exports = () => {
  return new Promise((resolve, reject) => {
    new WebpackDevServer(webpack(config), {
      contentBase: 'dist',
      publicPath: config.output.publicPath,
      hot: true,
      historyApiFallback: true,
      watchOptions: {
        ignored: /backend/
      }
    }).listen(WDSPort, 'localhost', function (err: any) {
      if (err) {
        return reject(err);
      }

      console.log(`Listening at http://localhost:${WDSPort}/`);

      return resolve();
    });
  });
};
