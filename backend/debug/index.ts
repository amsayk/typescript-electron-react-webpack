import { BrowserWindow } from 'electron';

const devtron = require('devtron');

const {
  default: installExtension,
  REACT_DEVELOPER_TOOLS,
  REDUX_DEVTOOLS
} = require('electron-devtools-installer');

/**
 * Add Debug functionality to the application.
 * Pass in the method that will start the app, and returns a BrowserWindow to bind to.
 * @param appStarter
 */
module.exports = (appStarter: () => Electron.BrowserWindow) => {
  if (process.env.HOT) {
    // Start HMR if desired
    require('./wdServer')()
      .then(installDevtron)
      .then(installDevTools.bind(undefined, appStarter))
      .then(debugComplete);
  } else {
    // Install Devtron and other extensions
    installDevtron();
    installDevTools(appStarter)
      .then(debugComplete);
  }
};

/**
 * Install Devtron Extension
 */
const installDevtron = () => {
  if (
    BrowserWindow.getDevToolsExtensions &&
    !BrowserWindow.getDevToolsExtensions().hasOwnProperty('devtron')
  ) {
    BrowserWindow.addDevToolsExtension(devtron.path);
  }
};

/**
 * Install remaining DevTools (React/Redux)
 * @param appStarter
 * @return {Promise<never | T>}
 */
const installDevTools = (appStarter: () => Electron.BrowserWindow) => {
  const Extensions = [
    REACT_DEVELOPER_TOOLS,
    REDUX_DEVTOOLS
  ];

  const forceDownload = !!process.env.UPGRADE_EXTENSIONS;

  return installExtension(Extensions, forceDownload)
    .then((devTools: Array<string>) => {
      console.log('Installed all Electron DevTools:', [ 'Devtron' ].concat(devTools));
    })
    .then(appStarter)
    .then((mainWindow: Electron.BrowserWindow) => {
      mainWindow.webContents.openDevTools({ mode: 'undocked' });
    })
    .catch(console.log);
};

const debugComplete = () => {
  console.log('Debug Added!');
};
