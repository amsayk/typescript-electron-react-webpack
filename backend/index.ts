require('./package.json'); // For webkit file-loader
import * as path from 'path';
import * as url from 'url';
import { app, BrowserWindow } from 'electron';

let mainWindow: Electron.BrowserWindow;
const Package = JSON.parse(require('fs').readFileSync('./package.json'));
const START_PROTOCOL = process.env.START_PROTOCOL;
const START_FILE = (process.env.START_FILE || '').indexOf('localhost') >= 0 ?
  process.env.START_FILE :
  path.resolve(__dirname, process.env.START_FILE);

/**
 * Handler for when the App is deemed Ready.
 */
const onReady = (): void => {
  if (process.env.NODE_ENV !== 'production') {
    require('./debug')(startApp);
  } else {
    startApp();
  }
};

/**
 * Start the Application!
 *
 * @returns {Electron.BrowserWindow}
 */
const startApp = (): Electron.BrowserWindow => {
  const fileName = url.format({
    pathname: START_FILE,
    protocol: START_PROTOCOL,
    slashes: true
  });

  mainWindow = new BrowserWindow({
    show: false,
    width: 450,
    height: 450
  });

  mainWindow.loadURL(fileName);
  mainWindow.on('ready-to-show', mainWindow.show);
  mainWindow.on('close', () => app.quit());

  return mainWindow;
};

app.on('ready', onReady);
// Remove if you plan on having background processes
app.on('window-all-closed', app.quit);

console.log(`Electron Version: ${process.versions.electron}`);
console.log(`App Version: ${Package.version}`);
